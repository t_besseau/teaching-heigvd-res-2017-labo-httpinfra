var Chance = require('chance');
var chance = new Chance();

var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send( generateCars() )
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

app.get('/test', function (req, res) {
  res.send('Hello RES - test is working')
});


function generateCars() {
	var nbCars = chance.natural({
		min: 1,
		max: 20
	});
	var cars = [];	
	
	for(var i = 0; i < nbCars; i++) {
		var carRand = chance.natural({
			min: 0,
			max: 6
		});	
		var carBrand;
		var color;
		var yearStartProduction;
		var yearEndProduction;
		var weightMin;
		var weightMax;
		var horsesPowerMin;
		var horsesPowerMax;

		
		switch (carRand) {
		case 0:
			carBrand = "Bugatti";
			color = "black";
			yearStartProduction = 1920;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 1700;
			horsesPowerMin = 80;
			horsesPowerMax= 1500;
			break;
		case 1:
			carBrand = "Jaguar";
			color = "green";
			yearStartProduction = 1950;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 2700;
			horsesPowerMin = 150;
			horsesPowerMax= 650;
			break;
		case 2:
			carBrand = "Ferrari";
			color = "red";
			yearStartProduction = 1930;
			yearEndProduction = 2017;
			weightMin = 1200;
			weightMax= 1700;
			horsesPowerMin = 301;
			horsesPowerMax= 850;
			break;
		case 3:
			carBrand = "Porsche";
			color = "grey";
			yearStartProduction = 1900;
			yearEndProduction = 2017;
			weightMin = 1000;
			weightMax= 1500;
			horsesPowerMin = 120;
			horsesPowerMax= 750;
			break;
		case 4:
			carBrand = "Aston Martin";
			color = "blue";
			yearStartProduction = 1950;
			yearEndProduction = 2017;
			weightMin = 1100;
			weightMax= 1700;
			horsesPowerMin = 300;
			horsesPowerMax= 660;
			break;
		case 5:
			carBrand = "Lamborghini";
			color = "yellow";
			yearStartProduction = 1935;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 2300;
			horsesPowerMin = 200;
			horsesPowerMax= 980;
			break;	
		case 6:
			carBrand = "Lexus";
			color = "white"
			yearStartProduction = 1960;
			yearEndProduction = 2017;
			weightMin = 1000;
			weightMax= 2200;
			horsesPowerMin = 300;
			horsesPowerMax= 500;
		}
		
		cars.push({
			brand: carBrand,
			color: color,
			weight: chance.natural({
				min: weightMin,
				max: weightMax
			}),
			horsePower: chance.natural({
				min: horsesPowerMin,
				max: horsesPowerMax
			}),
			year : chance.natural({
				min: yearStartProduction,
				max: yearEndProduction
			})
		});
	};
	return cars;
}