# Teaching-HEIGVD-RES-2017-Labo-HTTPInfra Rapport
### Authors : Thibaud Besseau
### Last update : 11.06.2017



## Step 1: Static HTTP server with apache httpd

### Create a docker image
For this first step, the objective was to be able to get an image of an apache server with php on dockerHub and display a static page with this image. 
To create our Docker image, we have created a DockerFile (in the folder docker-images) with the following lines: 

	FROM php:7.0-apache
	COPY src/ /var/www/html/
	
We have choose to use the latest PHP version because it's is more efficient than the 5.6.
On the second line, we can see that we use the folder src to put all website content. 
This content will be copy in /var/www/html/ when the image will be generate.



### Display a beautiful page
To display a beautiful page, we have found a beautiful bootstrap template on Github. We have forked this repo and remove some unnecessary sections.
We have put this template in our folder src/

### Build & Run 
After that, we have build our image with this command:

	docker build -t res/apache_php .

At that time, our image was ready to be launched with the following command

	docker run -p 9090:80 res/apache_php

### Test the website
Once the apache server started, you can try to access to your new web page:
In your web broswer tape:

	<your_local_server_ip_address>:9090
	
And normally the following page should be displayed:

![Static Web Page](/documentation_images/WebSite_Static.JPG)


## Step 2: Dynamic HTTP server with express.js

### Install Dependencies 
In this step we have created a new docker image named express-image. This image contain a node.js application.
This application has two dependencies (Chance & Express).
Chance is used to generate random content
Express is used like http server.

To install this two dependencies we have used theses following commands: 
npm install --save chance
npm install --save express

### Create nodeJs application
For this part, we have created a node application who return a json array with differents cars.
Our code is split in two parts:

The first one the http server who use express:

	var express = require('express');
	var app = express();

	app.get('/', function (req, res) {
	  res.send( generateCars() )
	});

	app.listen(3000, function () {
	  console.log('Example app listening on port 3000!');
	});

	app.get('/test', function (req, res) {
	  res.send('Hello RES - test is working')
	});

Like we can see, the server listen on the port 3000 and it makes two actions:
Send a json array generate by the function generateCars or display a test message if it receive a request with /test.

The second part is the generateCars function who generate a json array of cars with random informations

	function generateCars() {
	var nbCars = chance.natural({
		min: 1,
		max: 20
	});
	var cars = [];	
	
	for(var i = 0; i < nbCars; i++) {
		var carRand = chance.natural({
			min: 0,
			max: 6
		});	
		var carBrand;
		var color;
		var yearStartProduction;
		var yearEndProduction;
		var weightMin;
		var weightMax;
		var horsesPowerMin;
		var horsesPowerMax;

		
		switch (carRand) {
		case 0:
			carBrand = "Bugatti";
			color = "black";
			yearStartProduction = 1920;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 1700;
			horsesPowerMin = 80;
			horsesPowerMax= 1500;
			break;
		case 1:
			carBrand = "Jaguar";
			color = "green";
			yearStartProduction = 1950;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 2700;
			horsesPowerMin = 150;
			horsesPowerMax= 650;
			break;
		case 2:
			carBrand = "Ferrari";
			color = "red";
			yearStartProduction = 1930;
			yearEndProduction = 2017;
			weightMin = 1200;
			weightMax= 1700;
			horsesPowerMin = 301;
			horsesPowerMax= 850;
			break;
		case 3:
			carBrand = "Porsche";
			color = "grey";
			yearStartProduction = 1900;
			yearEndProduction = 2017;
			weightMin = 1000;
			weightMax= 1500;
			horsesPowerMin = 120;
			horsesPowerMax= 750;
			break;
		case 4:
			carBrand = "Aston Martin";
			color = "blue";
			yearStartProduction = 1950;
			yearEndProduction = 2017;
			weightMin = 1100;
			weightMax= 1700;
			horsesPowerMin = 300;
			horsesPowerMax= 660;
			break;
		case 5:
			carBrand = "Lamborghini";
			color = "yellow";
			yearStartProduction = 1935;
			yearEndProduction = 2017;
			weightMin = 1550;
			weightMax= 2300;
			horsesPowerMin = 200;
			horsesPowerMax= 980;
			break;	
		case 6:
			carBrand = "Lexus";
			color = "white"
			yearStartProduction = 1960;
			yearEndProduction = 2017;
			weightMin = 1000;
			weightMax= 2200;
			horsesPowerMin = 300;
			horsesPowerMax= 500;
		}
		
		cars.push({
			cars: carBrand,
			color: color,
			weight: chance.natural({
				min: weightMin,
				max: weightMax
			}),
			horsePower: chance.natural({
				min: horsesPowerMin,
				max: horsesPowerMax
			}),
			age: chance.natural({
				min: yearStartProduction,
				max: yearEndProduction
			})
		});
	};
	return cars;
}

### Build and Run our new image
After that, we have build our image with this command:

	docker build -t res/express_cars .

At that time, our image was ready to be launched with the following command

	docker run -p 9090:3000 res/express_cars
	
### Test our application
To test our application we have used a web browser and we have acceded to the following page:

	localhost:9090
	
### Result
On the page localhost:9090 we have got this result:
![Result JSON](/documentation_images/ResultJSON.JPG)

##Step 5: Dynamic reverse proxy configuration
In this last step, the goal is to get rid of hard-coded IP addresses in the reverse-proxy configuration. The configuration must become dynamic.

For this we use php like a script language. With PHP, we are able to recuperate environment variables.

### Create a config template
We have created a new directory template in apache-reverse-proxy. After we have created a php script to define the proxy informations.

	
=======
## Step 3: Reverse proxy with apache (static configuration)

In this part we have installed a single entry point to our HTTP servers.

### Running previous containers

We have launched the two containers created in the precedents steps with the following commands:

	docker run -d --name apache_static res/apache_php
	docker run -d --name express_dynamic res/express_cars

Once launched, we have used the following commands to get the ip address of each containers

	docker inpect apache_static | grep -i ipaddress
	docker inpect express_dynamic | grep -i ipaddress
	
Each time we run these two containers, we must check and add those ip addresses in the configuration file of the reverse proxy

### Create a new image for the reverse proxy 
We have created a new docker images called apache-reverse-proxy with the following DockerFile

	FROM php:7.0-apache
	COPY conf/ /etc/apache2
	RUN a2enmod proxy proxy_http
	RUN a2ensite 000-* 001-*
	
This Dockerfile create a docker image based on a php sever with apache image.
Once created, the DockerFile copy the additional configuration files contained in conf/ in /etc/apache2 and after the DockerFile apply this additional parameters.

### Configuration file for the reverse proxy 
We have create two files to modify the reverse proxy configuration

The first one set the default comportement when no parameters are received. When no param are given and when the server name is not used, the reverse display an error 403.

	<VirtualHost *:80>
	</VirtualHost>


The second, define which servers the reverse proxy need to redirect a request when he received a new one.

	<VirtualHost *:80>
		ServerName besseau.res.ch
		
		#ErrorLog ${APACHE_LOG_DIR}/error.log
		#CustomLog ${APACHE_LOG_DIR}/access.log combined
		
		ProxyPass "/api/cars/" "http://172.17.0.3:3000/"
		ProxyPassReverse "/api/cars/" "http://172.17.0.3:3000/"
		
		ProxyPass "/" "http://172.17.0.2:80/"
		ProxyPassReverse "/" "http://172.17.0.2:80/"
	</VirtualHost>
	
Like we can see all the requests, to obtain a list of cars are redirected on the dynamic server. When no paramaters are given in the URL it's the static server who respond.
The ip adresses of static and dynamic servers must be check on every restart.


### Test the reverse porxy 
Before to test, we have build and lauch the reverse proxy with the following command:

	docker build -t res/apache_reverse_proxy
	docker run -d -p 8080:80 res/apache_reverse_proxy

After that we have add the following URL in your host file config:

	127.0.0.1 besseau.res.ch
	
Now the reverse proxy is working 

If we access to the reverse by using the ip address and with no parameters
![Result Security](/documentation_images/reverseSecurity.jpg)

If we access to the static server
![Result Static](/documentation_images/ReversePart3Static.JPG)

If we access to the dynamic serveur
![Result Dynamic](/documentation_images/ReversePart3Dynamic.JPG)

## Step 4: AJAX requests with JQuery

### Add script into our static homepage
In index.html, we have added a script called cars.js with the following line:

	<script src="js/cars.js"> </script>
	
After we have created the script cars.js in the js directory:

	$(function(){
	console.log("Loding cars");
	
	function loadCars(){
		$.getJSON("/api/cars/", function(cars){
			console.log(cars);
			var message = "No cars in array";
			if(cars.length > 0)
			{
				message = cars[0].brand + " fabrique en " + cars[0].year;
			}
			$(".car").text(message);
		});
	};
	
	loadCars();
	setInterval(loadCars,2000);
	
	});
	
This script call every 2 secondes this URL /api/cars. Like explained before this url return a JSON array with a list of cars. 
We take only the first on and we display the brand et the year of production.

### The result 
![Result Ajax](/documentation_images/AjaxDisplay.JPG)


### Additionals informations
For security restriction reasons, it's necessary to use a reverse-proxy server to get the dynamic content from a another server. 
If you don't have one, you will have two diffrent domains: one for the static server and a second one for the dynamic and because all the browser block
cross-domain requests for security purposes you cannot use ajax without a reverse proxy.

## Step 5: Dynamic reverse proxy configuration
In this last step, the goal is to get rid of hard-coded IP addresses in the reverse-proxy configuration. The configuration must become dynamic.
For this we are going to use php like a script language.

### Config Template
For beginning, we have create a new folder nammed templates in apache-reverse-proxy. In this new directory, we have created a new file nammed config-template.php.
In this script, we have putted the file 001-reverse-proxy-conf inside, but we have added some changes.

	<?php
	$dynamic_app = getenv('DYNAMIC_APP');
	$static_app = getenv('STATIC_APP');
	?>
	<VirtualHost *:80>
		ServerName besseau.res.ch
		
		ProxyPass "/api/cars/" "http://<?php print "$dynamic_app";?>/"
		ProxyPassReverse "/api/cars/" "http://<?php print "$dynamic_app";?>/"
		
		ProxyPass "/" "http://<?php print "$static_app";?>/"
		ProxyPassReverse "/" "http://<?php print "$static_app";?>/"
	</VirtualHost>
	
Like you can see, we use PHP to get the addresses given by the user when he start the container and with theses addresses we generate the servers URLs.

### apache2-foreground
 To create this file, we have copy the same file than you can get in the [php 7 sources](https://github.com/docker-library/php/blob/master/7.0/apache/apache2-foreground).
In this file, we have added the following lines: 

	# Add setup for RES lab 
	echo "Setup for RES lab..."
	echo "Static app URL : $STATIC_APP"
	echo "Dynamic app URL : $DYNAMIC_APP"
	php /var/apache2/templates/config-template.php > /etc/apache2/sites-available/001-reverse-proxy.conf
	
In these lines only the last one is important because she overwrite the 001-reverse-proxy.conf file by our template that we have created before.

### Configure DockerFile
To finish the configuration it was necessary to make few change on the Dockerfile. We have added theses two lines who copy the files created before in the container.

	COPY apache2-foreground /usr/local/bin/
	COPY templates/ /var/apache2/templates/
	
### Lauch the container
One the server static and dynamic are launched, obtain their ip address by using theses commands
	
	docker inpect apache_static | grep -i ipaddress
	docker inpect express_dynamic | grep -i ipaddress
	
When you have this informations you can launch you reverse proxy by using this command

	docker run -d -e -p 8080:80 STATIC_APP=<ip static:port> -e DYNAMIC_APP=<ip dynamic:port> --name apache_reverse_proxy  res/apache_reverse_proxy
	
Now it's over :)